if (!Detector.webgl) Detector.addGetWebGLMessage();

var STATS_ENABLED = false;

var raycaster = new THREE.Raycaster();
var mouse = new THREE.Vector2();

var CARS = {

    "veyron": {

        name: "Bugatti Veyron",
        url: "obj/veyron/VeyronNoUv_bin.js",
        author: '<a href="http://artist-3d.com/free_3d_models/dnm/model_disp.php?uid=1129" target="_blank" rel="noopener">Troyano</a>',
        init_rotation: [0, 0, 0],
        scale: 5.5,
        init_material: 4,
        body_materials: [2],

        object: null,
        buttons: null,
        materials: null

    },

    "gallardo": {

        name: "Lamborghini Gallardo",
        url: "obj/gallardo/GallardoNoUv_bin.js",
        author: '<a href="http://artist-3d.com/free_3d_models/dnm/model_disp.php?uid=1711" target="_blank" rel="noopener">machman_3d</a>',
        init_rotation: [0, 0, 0],
        scale: 3.7,
        init_material: 9,
        body_materials: [3],

        object: null,
        buttons: null,
        materials: null

    },

    "f50": {

        name: "Ferrari F50",
        url: "obj/f50/F50NoUv_bin.js",
        author: '<a href="http://artist-3d.com/free_3d_models/dnm/model_disp.php?uid=1687" target="_blank" rel="noopener">daniel sathya</a>',
        init_rotation: [0, 0, 0],
        scale: 0.175,
        init_material: 2,
        body_materials: [3, 6, 7, 8, 9, 10, 23, 24],

        object: null,
        buttons: null,
        materials: null

    },

    "camaro": {

        name: "Chevrolet Camaro",
        url: "obj/camaro/CamaroNoUv_bin.js",
        author: '<a href="http://www.turbosquid.com/3d-models/blender-camaro/411348" target="_blank" rel="noopener">dskfnwn</a>',
        init_rotation: [0.0, 0.0, 0.0 /*0, 1, 0*/],
        scale: 75,
        init_material: 4,
        body_materials: [0],

        object: null,
        buttons: null,
        materials: null

    }

};

var WIDTH = window.innerWidth;
            var HEIGHT = window.innerHeight;

            // camera
            var VIEW_ANGLE = 45;
            var ASPECT = WIDTH / HEIGHT;
            var NEAR = 1;
            var FAR = 500;

var container, stats, controls, mesh, projector;

var camera, scene, renderer;

var m, mi;

var objects = [];

localStorage.setItem("componentValues", selectedValues);

var directionalLight, pointLight;

var mouseX = 0,
    mouseY = 0;

var windowHalfX = window.innerWidth / 2;
var windowHalfY = window.innerHeight / 2;

var loader = new THREE.BinaryLoader();

init();
animate();

function init() {

    container = document.createElement('div');
    document.body.appendChild(container);

    // CAMERAS

    camera = new THREE.PerspectiveCamera(38, window.innerWidth / window.innerHeight, 1, 100000);

    camera.position.x= -900;
    camera.position.y= 300;
    camera.position.z = 1500;

    controls = new THREE.TrackballControls(camera, container);

    // SCENE

    var textureCube = new THREE.CubeTextureLoader()
        .setPath('textures/cube/Bridge2/')
        .load(['posx.jpg', 'negx.jpg', 'posy.jpg', 'negy.jpg', 'posz.jpg', 'negz.jpg']);

    scene = new THREE.Scene();
    // scene.background = textureCube;
    // scene.background = new THREE.Color(0xffffff);

    // LIGHTS

    var ambient = new THREE.AmbientLight(0x050505);
    scene.add(ambient);

    directionalLight = new THREE.DirectionalLight(0xffffff, 2);
    directionalLight.position.set(2, 1.2, 10).normalize();
    scene.add(directionalLight);

    directionalLight = new THREE.DirectionalLight(0xffffff, 1);
    directionalLight.position.set(-2, 1.2, -10).normalize();
    scene.add(directionalLight);

    // pointLight = new THREE.PointLight(0xffaa00, 2);
    // pointLight.position.set(2000, 1200, 10000);
    // scene.add(pointLight);

    renderer = new THREE.WebGLRenderer({ alpha: true });
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.setFaceCulling(THREE.CullFaceNone);
    renderer.setClearColor( 0x000000, 0 );

    container.appendChild(renderer.domElement);

    if (STATS_ENABLED) {

        stats = new Stats();
        container.appendChild(stats.dom);

    }

    // document.addEventListener( 'mousemove', onDocumentMouseMove, false );

    container.addEventListener('mousedown', onDocumentMouseDown, false);

    // common materials

    var mlib = {

        "Orange": new THREE.MeshLambertMaterial({
            color: 0xff6600,
            // envMap: textureCube,
            combine: THREE.MixOperation,
            reflectivity: 0.3
        }),
        "Blue": new THREE.MeshLambertMaterial({
            color: 0x001133,
            // envMap: textureCube,
            combine: THREE.MixOperation,
            reflectivity: 0.3
        }),
        "Red": new THREE.MeshLambertMaterial({
            color: 0x660000,
            // envMap: textureCube,
            combine: THREE.MixOperation,
            reflectivity: 0.25,
        }),
        "Black": new THREE.MeshLambertMaterial({
            color: 0x000000,
            // envMap: textureCube,
            combine: THREE.MixOperation,
            reflectivity: 0.15
        }),
        "White": new THREE.MeshLambertMaterial({
            color: 0x336aa5,
            // envMap: textureCube,
            combine: THREE.MixOperation,
            reflectivity: 1,
        }),

        "Carmine": new THREE.MeshPhongMaterial({
            color: 0x770000,
            specular: 0xffaaaa,
            // envMap: textureCube,
            combine: THREE.MultiplyOperation
        }),
        "Gold": new THREE.MeshPhongMaterial({
            color: 0xaa9944,
            specular: 0xbbaa99,
            shininess: 50,
            // envMap: textureCube,
            combine: THREE.MultiplyOperation
        }),
        "Bronze": new THREE.MeshPhongMaterial({
            color: 0x150505,
            specular: 0xee6600,
            shininess: 10,
            // envMap: textureCube,
            combine: THREE.MixOperation,
            reflectivity: 0.25
        }),
        "Chrome": new THREE.MeshPhongMaterial({
            color: 0xffffff,
            specular: 0xffffff,
            // envMap: textureCube,
            combine: THREE.MultiplyOperation
        }),

        "Orange metal": new THREE.MeshLambertMaterial({
            color: 0xff6600,
            // envMap: textureCube,
            combine: THREE.MultiplyOperation
        }),
        "Blue metal": new THREE.MeshLambertMaterial({
            color: 0x001133,
            // envMap: textureCube,
            combine: THREE.MultiplyOperation
        }),
        "Red metal": new THREE.MeshLambertMaterial({
            color: 0x770000,
            // envMap: textureCube,
            combine: THREE.MultiplyOperation
        }),
        "Green metal": new THREE.MeshLambertMaterial({
            color: 0x007711,
            // envMap: textureCube,
            combine: THREE.MultiplyOperation
        }),
        "Black metal": new THREE.MeshLambertMaterial({
            color: 0x222222,
            // envMap: textureCube,
            combine: THREE.MultiplyOperation
        }),

        "Pure chrome": new THREE.MeshLambertMaterial({
            color: 0xffffff,
            // envMap: textureCube
        }),
        "Dark chrome": new THREE.MeshLambertMaterial({
            color: 0x444444,
            // envMap: textureCube
        }),
        "Darker chrome": new THREE.MeshLambertMaterial({
            color: 0x222222,
            // envMap: textureCube
        }),

        "Black glass": new THREE.MeshLambertMaterial({
            color: 0x101016,
            // envMap: textureCube,
            opacity: 0.975,
            transparent: true
        }),
        "Dark glass": new THREE.MeshLambertMaterial({
            color: 0x101046,
            // envMap: textureCube,
            opacity: 0.25,
            transparent: true
        }),
        "Blue glass": new THREE.MeshLambertMaterial({
            color: 0x668899,
            // envMap: textureCube,
            opacity: 0.75,
            transparent: true
        }),
        "Light glass": new THREE.MeshBasicMaterial({
            color: 0x223344,
            // envMap: textureCube,
            opacity: 0.25,
            transparent: true,
            combine: THREE.MixOperation,
            reflectivity: 0.25
        }),

        "Red glass": new THREE.MeshLambertMaterial({
            color: 0xff0000,
            opacity: 0.75,
            transparent: true
        }),
        "Yellow glass": new THREE.MeshLambertMaterial({
            color: 0xffffaa,
            opacity: 0.75,
            transparent: true
        }),
        "Orange glass": new THREE.MeshLambertMaterial({
            color: 0x995500,
            opacity: 0.75,
            transparent: true
        }),

        "Orange glass 50": new THREE.MeshLambertMaterial({
            color: 0xffbb00,
            opacity: 0.5,
            transparent: true
        }),
        "Red glass 50": new THREE.MeshLambertMaterial({
            color: 0xff0000,
            opacity: 0.5,
            transparent: true
        }),

        "Fullblack rough": new THREE.MeshLambertMaterial({
            color: 0x000000
        }),
        "Black rough": new THREE.MeshLambertMaterial({
            color: 0x050505
        }),
        "Darkgray rough": new THREE.MeshLambertMaterial({
            color: 0x090909
        }),
        "Red rough": new THREE.MeshLambertMaterial({
            color: 0x330500
        }),

        "Darkgray shiny": new THREE.MeshPhongMaterial({
            color: 0x000000,
            specular: 0x050505
        }),
        "Gray shiny": new THREE.MeshPhongMaterial({
            color: 0x050505,
            shininess: 20
        })

    };

    // Gallardo materials

    CARS["gallardo"].materials = {

        body: [

            ["Orange", mlib["Orange"]],
            ["Blue", mlib["Blue"]],
            ["Red", mlib["Red"]],
            ["Black", mlib["Black"]],
            ["White", mlib["White"]],

            ["Orange metal", mlib["Orange metal"]],
            ["Blue metal", mlib["Blue metal"]],
            ["Green metal", mlib["Green metal"]],
            ["Black metal", mlib["Black metal"]],

            ["Carmine", mlib["Carmine"]],
            ["Gold", mlib["Gold"]],
            ["Bronze", mlib["Bronze"]],
            ["Chrome", mlib["Chrome"]]

        ]

    };

    m = CARS["gallardo"].materials;
    mi = CARS["gallardo"].init_material;

    CARS["gallardo"].mmap = {

        0: mlib["Pure chrome"], // wheels chrome
        1: mlib["Black rough"], // tire
        2: mlib["Black glass"], // windshield
        3: m.body[mi][1], // body
        4: mlib["Red glass"], // back lights
        5: mlib["Yellow glass"], // front lights
        6: mlib["Dark chrome"] // windshield rim

    };

    // Veyron materials

    CARS["veyron"].materials = {

        body: [

            ["Orange metal", mlib["Orange metal"]],
            ["Blue metal", mlib["Blue metal"]],
            ["Red metal", mlib["Red metal"]],
            ["Green metal", mlib["Green metal"]],
            ["Black metal", mlib["Black metal"]],

            ["Gold", mlib["Gold"]],
            ["Bronze", mlib["Bronze"]],
            ["Chrome", mlib["Chrome"]]

        ]

    };

    m = CARS["veyron"].materials;
    mi = CARS["veyron"].init_material;

    CARS["veyron"].mmap = {

        0: mlib["Black rough"], // tires + inside
        1: mlib["Pure chrome"], // wheels + extras chrome
        2: m.body[mi][1], // back / top / front torso
        3: mlib["Dark glass"], // glass
        4: mlib["Pure chrome"], // sides torso
        5: mlib["Pure chrome"], // engine
        6: mlib["Red glass 50"], // backlights
        7: mlib["Orange glass 50"] // backsignals

    };

    // F50 materials

    CARS["f50"].materials = {

        body: [

            ["Orange", mlib["Orange"]],
            ["Blue", mlib["Blue"]],
            ["Red", mlib["Red"]],
            ["Black", mlib["Black"]],
            ["White", mlib["White"]],

            ["Orange metal", mlib["Orange metal"]],
            ["Blue metal", mlib["Blue metal"]],
            ["Black metal", mlib["Black metal"]],

            ["Carmine", mlib["Carmine"]],
            ["Gold", mlib["Gold"]],
            ["Bronze", mlib["Bronze"]],
            ["Chrome", mlib["Chrome"]]

        ]

    };

    m = CARS["f50"].materials;
    mi = CARS["f50"].init_material;

    CARS["f50"].mmap = {

        0: mlib["Dark chrome"], // interior + rim
        1: mlib["Pure chrome"], // wheels + gears chrome
        2: mlib["Blue glass"], // glass
        3: m.body[mi][1], // torso mid + front spoiler
        4: mlib["Darkgray shiny"], // interior + behind seats
        5: mlib["Darkgray shiny"], // tiny dots in interior
        6: m.body[mi][1], // back torso
        7: m.body[mi][1], // right mirror decal
        8: m.body[mi][1], // front decal
        9: m.body[mi][1], // front torso
        10: m.body[mi][1], // left mirror decal
        11: mlib["Pure chrome"], // engine
        12: mlib["Darkgray rough"], // tires side
        13: mlib["Darkgray rough"], // tires bottom
        14: mlib["Darkgray shiny"], // bottom
        15: mlib["Black rough"], // ???
        16: mlib["Orange glass"], // front signals
        17: mlib["Dark chrome"], // wheels center
        18: mlib["Red glass"], // back lights
        19: mlib["Black rough"], // ???
        20: mlib["Red rough"], // seats
        21: mlib["Black rough"], // back plate
        22: mlib["Black rough"], // front light dots
        23: m.body[mi][1], // back torso
        24: m.body[mi][1] // back torso center

    };


    // Camero materials

    CARS["camaro"].materials = {

        body: [

            ["Orange", mlib["Orange"]],
            ["Blue", mlib["Blue"]],
            ["Red", mlib["Red"]],
            ["Black", mlib["Black"]],
            ["White", mlib["White"]],

            ["Orange metal", mlib["Orange metal"]],
            ["Blue metal", mlib["Blue metal"]],
            ["Red metal", mlib["Red metal"]],
            ["Green metal", mlib["Green metal"]],
            ["Black metal", mlib["Black metal"]],

            ["Gold", mlib["Gold"]],
            ["Bronze", mlib["Bronze"]],
            ["Chrome", mlib["Chrome"]]

        ]

    };

    m = CARS["camaro"].materials;
    mi = CARS["camaro"].init_material;

    CARS["camaro"].mmap = {

        0: m.body[mi][1], // car body
        1: mlib["Pure chrome"], // wheels chrome
        2: mlib["Pure chrome"], // grille chrome
        3: mlib["Dark chrome"], // door lines
        4: mlib["Light glass"], // windshield
        5: mlib["Gray shiny"], // interior
        6: mlib["Black rough"], // tire
        7: mlib["Fullblack rough"], // tireling
        8: mlib["Fullblack rough"] // behind grille

    };

    loader.load(CARS["camaro"].url, function (geometry) {
        createScene(geometry, "camaro")
    });

    for (var c in CARS) initCarButton(c);

    //

    window.addEventListener('resize', onWindowResize, false);

    projector = new THREE.Projector();

}

function onWindowResize() {

    windowHalfX = window.innerWidth / 2;
    windowHalfY = window.innerHeight / 2;

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize(window.innerWidth, window.innerHeight);

}

function initCarButton(car) {

    // $(car).addEventListener('click', function () {

    //     if (!CARS[car].object) {

    //         loader.load(CARS[car].url, function (geometry) {
    //             createScene(geometry, car)
    //         });

    //     } else {

    //         switchCar(car);

    //     }

    // }, false);

}

// function $(id) {
//     return document.getElementById(id)
// }

function button_name(car, index) {
    return "m_" + car + "_" + index
}

function switchCar(car) {

    for (var c in CARS) {

        if (c != car && CARS[c].object) {

            CARS[c].object.visible = false;
            CARS[c].buttons.style.display = "none";

        }
    }

    CARS[car].object.visible = true;
    CARS[car].buttons.style.display = "block";

    // $("car_name").innerHTML = CARS[car].name + " model";
    // $("car_author").innerHTML = CARS[car].author;

}

function createButtons(materials, car) {

    var buttons, i, src = "";

    for (i = 0; i < materials.length; i++) {

        src += '<button id="' + button_name(car, i) + '">' + materials[i][0] + '</button> ';

    }

    buttons = document.createElement("div");
    buttons.innerHTML = src;

    // $("buttons_materials").appendChild(buttons);

    return buttons;

}

function attachButtonMaterials(materials, faceMaterials, material_indices, car) {

    for (var i = 0; i < materials.length; i++) {

        $(button_name(car, i)).counter = i;
        $(button_name(car, i)).addEventListener('click', function () {

            for (var j = 0; j < material_indices.length; j++) {

                faceMaterials[material_indices[j]] = materials[this.counter][1];

            }

        }, false);

    }

}

function createScene(geometry, car) {

    geometry.sortFacesByMaterialIndex();

    var m = [],
        s = CARS[car].scale * 1,
        r = CARS[car].init_rotation,
        materials = CARS[car].materials,
        mi = CARS[car].init_material,
        bm = CARS[car].body_materials;

    for (var i in CARS[car].mmap) {

        m[i] = CARS[car].mmap[i];

    }

    var mesh = new THREE.Mesh(geometry, m);

    mesh.rotation.x = r[0];
    mesh.rotation.y = r[1];
    mesh.rotation.z = r[2];

    mesh.scale.x = mesh.scale.y = mesh.scale.z = s;

    scene.add(mesh);

    CARS[car].object = mesh;

    CARS[car].buttons = createButtons(materials.body, car);
    // attachButtonMaterials(materials.body, m, bm, car);

    var geometry1 = new THREE.CubeGeometry(10, 10, 10);

    switchCar(car);

}

function addShape(shape, color, x, y, z, rx, ry, rz, s) {

    // flat shape

    var geometry = new THREE.ShapeGeometry(shape);
    var material = new THREE.MeshBasicMaterial({ color: color, overdraw: 1 });

    var mesh = new THREE.Mesh(geometry, material);
    mesh.position.set(x, y, z);
    mesh.rotation.set(rx, ry, rz);
    mesh.scale.set(s, s, s);
    return mesh;

}

function onDocumentMouseMove(event) {

    mouseY = (event.clientY - window.innerHeight);

}

function onDocumentMouseDown(event) {
    event.preventDefault();
    var vector = new THREE.Vector3((event.clientX / window.innerWidth) * 2 -
        1, -(event.clientY / window.innerHeight) * 2 + 1, 0.5);
    projector.unprojectVector(vector, camera);
    // var raycaster = projector.pickingRay( vector, camera );
    var raycaster = new THREE.Raycaster(camera.position, vector.sub(camera.position).normalize());
    // raycaster.setFromCamera( vector, camera);
    var intersects = raycaster.intersectObjects(objects);
        
    if (intersects.length > 0) {
        $("#part_details").hide("slide", {direction: "right"}, 200);
        // var points = toScreenPosition(intersects[0].object, camera);
        // var text2 = document.createElement('div');
        // text2.style.position = 'absolute';
        //text2.style.zIndex = 1;    // if you still don't see the label, try uncommenting this
        // text2.style.width = 100;
        // text2.style.height = 100;
        // text2.style.backgroundColor = "blue";
        // text2.innerHTML = "hi there!";
        // text2.style.top = event.clientY + 'px';
        // text2.style.left = event.clientX + 'px';
        // document.body.appendChild(text2);


        var checked = false;
        var hasManualEntry = false;        
        var partName = intersects[0].object.userData.part;
        var option = intersects[0].object.userData.option;
        var heading = $('.panel-heading')
        var content = $('.panel-body')
        // heading.innerHTML = "Z=" + parts[option][partName].z;
        $('.panel-heading')[0].innerHTML = partName;
        var table = '<div style="margin-bottom:10px"><strong>Z=' + parts[option][partName].z + '</strong></div><table class="table-bordered"><tbody><tr>';
        table += '<td>' + option + '</td>';

        Object.keys(parts[option][partName]).forEach(function (element) {
            if (element != "z") {
                table += '<td>' + element + '</td>';
            }
        });
        table += '</tr><tr><td>IT</td>';
        Object.keys(parts[option][partName]).forEach(function (value) {
            if (value != "z") {
                table += "<td><div class='btn-group'>";
                parts[option][partName][value].forEach(function (element) {

                    if(selectedValues[option]){
                        if(selectedValues[option][partName]){
                            if(selectedValues[option][partName][value]){
                                if(selectedValues[option][partName][value] == element.toString()){
                                    checked = true;
                                }
                                else if(selectedValues[option][partName][value] != ""){
                                    hasManualEntry = true;
                                }                                
                            }
                        }
                    }

                    if(checked){
                        table += '<label class="btn btn-primary options">'
                        + '<input checked="checked" onClick="storeValues(\'' + option + '\', \'' + partName + '\', \'' + value + '\', \'' + element + '\')" type="radio" name="options_' + value + '"'
                        + 'id="' + value + "_" + element + '" autocomplete="off"><span>' + element + '</span></label>';
                    }
                    else{
                        table += '<label class="btn btn-primary options">'
                        + '<input onClick="storeValues(\'' + option + '\', \'' + partName + '\', \'' + value + '\', \'' + element + '\')" type="radio" name="options_' + value + '"'
                        + 'id="' + value + "_" + element + '" autocomplete="off"><span>' + element + '</span></label>';
                    }
                    checked = false;                  
                });
                if(hasManualEntry){
                    table += '<input value="'+selectedValues[option][partName][value]+'" onchange="storeManualValues(event, \'' + option + '\', \'' + partName + '\', \'' + value + '\')" class="form-control" type="text" placeholder="Manual Entry"></div></td>';
                }                
                else{
                    table += '<input onchange="storeManualValues(event, \'' + option + '\', \'' + partName + '\', \'' + value + '\')" class="form-control" type="text" placeholder="Manual Entry"></div></td>';
                }
                hasManualEntry = false;
                
            }
        });
        table += '</tbody></table>';
        $('.panel-body')[0].innerHTML = table;
        $("#part_details").show("slide", {direction: "right"}, "slow");
        // var divVector = toScreenPosition(vector);
    }
}

function toScreenPosition(vector) {
    console.log(vector);
    // TODO: need to update this when resize window
    var widthHalf = 1 * renderer.context.canvas.width;
    var heightHalf = 1 * renderer.context.canvas.height;

    vector.x = (vector.x * widthHalf) + widthHalf;
    vector.y = -(vector.y * heightHalf) + heightHalf;

    return {
        x: vector.x,
        y: vector.y
    };

}


//

function animate() {
    controls.update();

    requestAnimationFrame(animate);

    render();

}

function render() {

    // var timer = -0.0002 ;
    // camera.position.x = 1000 * Math.cos( timer );
    // camera.position.y += ( - mouseY - camera.position.y ) * .05;
    // camera.position.z = 1000 * Math.sin( timer );

    camera.lookAt(scene.position);

    renderer.render(scene, camera);

    if (STATS_ENABLED) stats.update();

}

function createGapPoints(ev, option) {
    // $("#part_details").hide("slide");
    $("#part_details").hide("slide", {direction: "right"}, 200);
    // scene.children = [];
    // loader.load(CARS["veyron"].url, function (geometry) {
    //     createScene(geometry, "veyron")
    // });
    var element = ev.srcElement;
    var color = "";
    if (option == 'Gap') {
        element.className += " selected-gap";
        color = 0x008000;
        ev.srcElement.parentElement.nextElementSibling.firstElementChild.classList.remove("selected-flush")
    }
    else {
        element.className += " selected-flush";
        color = 0xffff00;
        ev.srcElement.parentElement.previousElementSibling.firstElementChild.classList.remove("selected-gap")
    }
    objects = [];
    // var geometry = new THREE.CircleBufferGeometry( 5, 32 );
    // var material = new THREE.MeshBasicMaterial( { color: 0xfffff } );
    // var circle = new THREE.Mesh( geometry, material );
    // scene.add( circle );
    // objects.push(circle);
    for (var i = 0; i < 5; i++) {
        var texLoader = new THREE.TextureLoader();
        map = texLoader.load('textures/sprites/disc.png');
        var circleRadius = 10;
        var circleShape = new THREE.Shape();
        circleShape.moveTo( 50, 10 );
        circleShape.absarc( 5, 5, 5, 0, Math.PI * 2, false );
        // circleShape.moveTo(0, circleRadius);
        // circleShape.quadraticCurveTo(circleRadius, circleRadius, circleRadius, 0);
        // circleShape.quadraticCurveTo(circleRadius, -circleRadius, 0, -circleRadius);
        // circleShape.quadraticCurveTo(-circleRadius, -circleRadius, -circleRadius, 0);
        // circleShape.quadraticCurveTo(-circleRadius, circleRadius, 0, circleRadius);
        // // if (i == 0) {
        //     var texSprite = addShape(circleShape, color, 100, 200, 0, 300, 0, 0, 1);
        //     texSprite.userData = {
        //         part: "Part1"
        //     };
        //     scene.add(texSprite)
        //     objects.push(texSprite);

        // } else if (i == 1) {
        //     var texSprite = addShape(circleShape, color, 200, 20, 600, 100, 0, 0, 1);
        //     texSprite.userData = {
        //         part: "Part2"
        //     };
        //     scene.add(texSprite)
        //     objects.push(texSprite);
        // } else if (i == 2) {
        //     var texSprite = addShape(circleShape, color, -200, 20, 600, 100, 0, 0, 1);
        //     texSprite.userData = {
        //         part: "Part3"
        //     };
        //     scene.add(texSprite)
        //     objects.push(texSprite);
        // } else if (i == 3) {
        //     var texSprite = addShape(circleShape, color, -100, 200, 0, 300, 0, 0, 1);
        //     texSprite.userData = {
        //         part: "Part4"
        //     };
        //     scene.add(texSprite)
        //     objects.push(texSprite);
        // } 
        // else if (i == 4) {
        //     var texSprite = addShape(circleShape, color, 350, 0, 100, 0, -300, 0, 1);
        //     texSprite.userData = {
        //         part: "Part5"
        //     };
        //     scene.add(texSprite)
        //     objects.push(texSprite);
        // }
        if (i == 1) {
            var texSprite = addShape(circleShape, color, -300, -10, 290, 200, 300, 0, 1);
            texSprite.userData = {
                option: option,
                part: "Part1"
            };
            scene.add(texSprite)
            objects.push(texSprite);
        }
        else if (i == 2) {
            var texSprite = addShape(circleShape, color, -290, 40, 220, 200, 300, 0, 1);
            texSprite.userData = {
                option: option,
                part: "Part2"
            };
            scene.add(texSprite)
            objects.push(texSprite);
        }
        // else if (i == 6) {
        //     var texSprite = addShape(circleShape, color, 320, 100, -400, 100, -200, 100, 1);
        //     texSprite.userData = {
        //         part: "Part7"
        //     };
        //     scene.add(texSprite)
        //     objects.push(texSprite);
        // }
        else if (i == 3) {
            var texSprite = addShape(circleShape, color, -230, 150, -210, 100, 200, 100, 1);
            texSprite.userData = {
                option: option,
                part: "Part3"
            };
            scene.add(texSprite)
            objects.push(texSprite);
        }
        else if (i == 4) {
            var texSprite = addShape(circleShape, color, -260, 100, -200, 100, 200, 100, 1);
            texSprite.userData = {
                option: option,
                part: "Part4"
            };
            scene.add(texSprite)
            objects.push(texSprite);
        }
    }
    // render();          
}

function storeValues(option, partName, value, element) {
    if (selectedValues[option]) {
        if (selectedValues[option][partName]) {
            if (selectedValues[option][partName][value]) {
                selectedValues[option][partName][value] = element;
            }
            else {
                selectedValues[option][partName][value] = {};
                selectedValues[option][partName][value] = element;
            }
        }
        else {
            selectedValues[option][partName] = {};
            selectedValues[option][partName][value] = {};
            selectedValues[option][partName][value] = element;
        }
    }
    else {

        selectedValues[option] = {};
        selectedValues[option][partName] = {};
        selectedValues[option][partName][value] = {};
        selectedValues[option][partName][value] = element;
    }
    localStorage.setItem("componentValues", selectedValues);  
}

function storeManualValues(event, option, partName, value){
    console.log(event);
    if (selectedValues[option]) {
        if (selectedValues[option][partName]) {
            if (selectedValues[option][partName][value]) {
                selectedValues[option][partName][value] = event.srcElement.value;
            }
            else {
                selectedValues[option][partName][value] = {};
                selectedValues[option][partName][value] = event.srcElement.value;
            }
        }
        else {
            selectedValues[option][partName] = {};
            selectedValues[option][partName][value] = {};
            selectedValues[option][partName][value] = event.srcElement.value;
        }
    }
    else {

        selectedValues[option] = {};
        selectedValues[option][partName] = {};
        selectedValues[option][partName][value] = {};
        selectedValues[option][partName][value] = event.srcElement.value;
    }

}

function toScreenPosition(object, camera)
{
    object.updateMatrixWorld();
    var vector = object.position.clone();

    vector.applyMatrix4( object.matrixWorld );
    return vector;

};